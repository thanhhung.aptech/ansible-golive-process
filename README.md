Generate ansible script from yaml input and public steps


- Edit generate-playbook.yml

- update ansible_user, ansible_password, path generate (/opt/ansible-golive-process/golive_workflow-playbook.yml)

- update hosts.yml

- change input data with input-1.yaml

- Run command: ansible-playbook generate-playbook.yml -i hosts.yml -e '{"my_username":"username", "my_password":"password"}'
